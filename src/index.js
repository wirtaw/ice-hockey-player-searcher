'use strict';

const Connection = require('./modules/connection');

const con = new Connection('Kevin Stevens');
// console.dir(con, {depth: 3});

con.getWiki(true, () => {});

con.getWiki(false, () => {});

// con.getHockeyDB(() => {});

setTimeout(() => {
  console.dir(con.handlerWiki.playerInfo, {depth: 1});
  // console.dir(con.handlerHockeyDB.playerInfo, {depth: 1});
}, 10000);
