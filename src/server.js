'use strict';

const http = require('node:http');
const port = process.env.PORT || 8080;

const manage = require('./modules/manageRequest');

const requestHandler = async (request, response) => {
  let body = [];
  // console.info(`request.method ${request.method}`);
  // debugger;
  const {headers, statusCode} = manage.prepareHeaders(request);

  // console.info(`statusCode ${statusCode}`);
  // console.info(`headers ${JSON.stringify(headers)}`);

  if (request.method === 'POST') {
    request.on('data', chunk => {
      body.push(chunk);
    });

    request.on('end', async () => {
      body = Buffer.concat(body).toString('utf8');

      const {content} = await manage.prepareContent(request, body);
      // console.info(`content ${content}`);

      Object.keys(headers).forEach(key => {
        response.setHeader(key, headers[key]);
      });
      response.writeHead(statusCode);

      response.end(content);
    });
  } else {
    Object.keys(headers).forEach(key => {
      response.setHeader(key, headers[key]);
    });
    response.writeHead(statusCode);

    const {content} = await manage.prepareContent(request, null);

    response.end(content);
  }
};

const server = http.createServer(requestHandler);

server.listen(port, (err) => {
  if (err) {
    return console.info('something bad happened', err);
  }

  console.info(`server is listening on ${port}`);
});

module.exports = server;
