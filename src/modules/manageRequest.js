'use strict';

const fs = require('fs');
const path = require('path');
const {parse} = require('querystring');
const pug = require('pug');
const urlM = require('url');

const Connection = require('./connection');

const HEADERS = {};
let STATUS_CODE = 200;
let type = 'index';

const getHeader = () => {
  if (type === 'search') {
    HEADERS['Content-Type'] = 'text/html';
    HEADERS['Cache-Control'] = 'max-age=31536000';
  } else if (type === 'bootstrapcss') {
    HEADERS['Content-Type'] = 'text/css';
    HEADERS['Cache-Control'] = 'max-age=31536000';
  } else if (type === 'jquery') {
    HEADERS['Content-Type'] = 'application/javascript';
    HEADERS['Cache-Control'] = 'max-age=31536000';
  } else if (type === 'jqueryslim') {
    HEADERS['Content-Type'] = 'application/javascript';
    HEADERS['Cache-Control'] = 'max-age=31536000';
  } else if (type === 'bootstrapjs') {
    HEADERS['Content-Type'] = 'application/javascript';
    HEADERS['Cache-Control'] = 'max-age=31536000';
  } else if (type === 'favicon') {
    HEADERS['Content-Type'] = 'image/x-icon';
    HEADERS['Cache-Control'] = 'max-age=31536000';
  } else if (type === 'index') {
    HEADERS['Content-Type'] = 'text/html';
    HEADERS['Cache-Control'] = 'max-age=31536000';
  } else {
    HEADERS['Content-Type'] = 'text/html';
    HEADERS['Cache-Control'] = 'max-age=31536000';
  }
};

const getStatusCode = () => {
  if (type === 'notfound') {
    STATUS_CODE = 404;
  } else {
    STATUS_CODE = 200;
  }
};

const getType = (request) => {
  if (request.url.indexOf('/search') > -1) {
    type = 'search';
  } else if (request.url.indexOf('bootstrap.min.css') > -1) {
    type = 'bootstrapcss';
  } else if (request.url.indexOf('jquery.min.js') > -1) {
    type = 'jquery';
  } else if (request.url.indexOf('jquery.slim.min.js') > -1) {
    type = 'jqueryslim';
  } else if (request.url.indexOf('bootstrap.min.js') > -1) {
    type = 'bootstrapjs';
  } else if (request.url.indexOf('favicon') > -1) {
    type = 'favicon';
  } else if (request.url === '/' && (request.method === 'POST' || request.method === 'GET')) {
    type = 'index';
  } else {
    type = 'notfound';
  }
};

const prepareHeaders = (request) => {
  getType(request);
  getHeader();
  getStatusCode();
  return {headers: HEADERS, statusCode: STATUS_CODE};
};

const getPage = (source) => {
  let body = '';

  if (type === 'index') {
    body = pug.renderFile(path.join(__dirname, './../templates/index.pug'), {
      title: 'Input player name',
      copyright: `poplauki &copy; ${(new Date()).getFullYear()}`,
    });
  } else if (source && type === 'search') {
    body = pug.renderFile(path.join(__dirname, './../templates/result.pug'), {
      title: 'Search result',
      copyright: `poplauki &copy; ${(new Date()).getFullYear()}`,
      data: source,
    });
  } else if (type === 'bootstrapcss') {
    body = fs.readFileSync(
      path.join(__dirname, './../../scripts/bootstrap/4.4.1/css/bootstrap.min.css'),
      {encoding: 'utf8'},
    );
  } else if (type === 'jquery') {
    body = fs.readFileSync(
      path.join(__dirname, './../../scripts/jquery/3.4.1/jquery.min.js'),
      {encoding: 'utf8'},
    );
  } else if (type === 'jqueryslim') {
    body = fs.readFileSync(
      path.join(__dirname, './../../scripts/jquery/3.4.1/jquery.slim.min.js'),
      {encoding: 'utf8'},
    );
  } else if (type === 'bootstrapjs') {
    body = fs.readFileSync(
      path.join(__dirname, './../../scripts/bootstrap/4.4.1/js/bootstrap.min.js'),
      {encoding: 'utf8'},
    );
  } else if (type === 'favicon') {
    body = fs.readFileSync(
      path.join(__dirname, './../../images/favicon.ico'),
      {encoding: 'utf8'},
    );
  } else if (type === 'notfound') {
    body = pug.renderFile(path.join(__dirname, './../templates/error404.pug'), {
      title: 'Page not found',
      copyright: `poplauki &copy; ${(new Date()).getFullYear()}`,
    });
  } else if (!source && type === 'search') {
    body = pug.renderFile(path.join(__dirname, './../templates/result.pug'), {
      title: 'Search result',
      copyright: `poplauki &copy; ${(new Date()).getFullYear()}`,
      data: [{error: 'Form request is empty'}],
    });
  }

  return body;
};

const prepareContent = async (request, data) => {
  getType(request);

  // console.dir(request, {depth: 1});

  if (data && request.method === 'POST') {
    // console.info(`request.method POST type ${type}`);
    try {
      let obj;
      if (data.indexOf('{') > -1) {
        obj = Object.assign({}, JSON.parse(data));
      } else {
        obj = Object.assign({}, parse(data));
      }
      const playerName = (obj.playerName) ? obj.playerName : '';

      const con = new Connection(playerName);

      // console.dir(playerName, {depth: 2});
      // console.dir(con.names, {depth: 2});
      if (con.names === null) {
        return {content: getPage()};
      }

      return new Promise(resolve => {
        con.getPlayerInfo(results => {
          getType(request);
          // console.dir(results, {depth: 3});
          resolve({content: getPage(results)});
        });
      });
    } catch (e) {
      return {content: getPage([{error: 'Parse data error'}])};
    }
  } else if (request.method === 'GET') {
    getType(request);

    if (type === 'search') {
      const {query} = urlM.parse(request.url);
      if (query) {
        const {playerName} = parse(query);
        const con = new Connection(playerName);

        // console.dir(con, {depth: 2});
        if (con.names === null) {
          return {content: getPage()};
        }
        return new Promise((resolve) => {
          con.getPlayerInfo(results => {
            getType(request);

            console.dir(results, {depth: 1});

            resolve({content: getPage(results)});
          });
        });
      } else {
        return {content: getPage()};
      }
    } else {
      return {content: getPage()};
    }
  } else {
    return {content: null};
  }
};

module.exports = {prepareHeaders, prepareContent};
