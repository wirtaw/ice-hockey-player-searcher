'use strict';

class Player {
  constructor(names) {
    if (this.schema(names)) {
      const {firstName, lastName} = names;
      this.name = firstName;
      this.surname = lastName;
    } else {
      this.name = '';
      this.surname = '';
    }
    this.born = null;
    this.height = 0;
    this.weight = 0;
    this.shot = '';
    this.position = '';
    this.source = '';
  }

  setBorn(stringBorn) {
    this.born = stringBorn;
  }

  setHeight(stringHeight) {
    this.height = stringHeight;
  }

  setWeight(stringWeight) {
    this.weight = stringWeight;
  }

  setShot(stringShot) {
    this.shot = stringShot;
  }

  setPosition(stringPosition) {
    this.position = stringPosition;
  }

  setSource(stringSource) {
    this.source = stringSource;
  }

  show() {
    console.group('PlayerInfo');
    console.info(` Player ${this.name} ${this.surname}`);
    if (this.checkEmpty()) {
      console.info(` Born ${this.born} `);
      console.info(` Height ${this.height} `);
      console.info(` Weight ${this.weight} `);
      console.info(` Shot ${this.shot} `);
      console.info(` Position ${this.position} `);
    }
    console.groupEnd('PlayerInfo');
  }

  clear() {
    this.born = null;
    this.height = 0;
    this.weight = 0;
    this.shot = '';
    this.position = '';
  }

  checkEmpty() {
    return this.born || this.height || this.weight || this.shot || this.position;
  }

  schema(names) {
    let res = false;

    if (names && names.firstName && names.lastName) {
      res = true;
    }

    return res;
  }
}

module.exports = Player;
