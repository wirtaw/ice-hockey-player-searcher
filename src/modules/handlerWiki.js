'use strict';

const cheerio = require('cheerio');
const {DateTime} = require('luxon');

const Handler = require('./handler');

class HandlerWiki extends Handler {
  cleanStrings(str) {
    return str.trim().replace('\n', '').replace('\r', '');
  }

  getRowText(domItem) {
    const result = [];
    let i = 0;

    // console.group('domItem');

    while (i < domItem.children.length) {
      let text = '';
      let children = domItem.children[i];

      if (children && children.data) {
        text = this.cleanStrings(children.data);
        result.push(text);
      }

      // console.log(text, i);
      let j = 0;
      while (text === '' && (children && children.children && children.children.length > 0)) {
        children = children.children[j];

        if (children && children.data) {
          text = this.cleanStrings(children.data) || '';
          result.push(text);
        }
        j = j + 1;
      }

      i = i + 1;
    }

    // console.info('result ', result);
    // console.groupEnd('domItem');

    return result.filter(text => text.trim());
  }

  parse() {
    // console.log($('table.infobox', this.responseBody).length);
    // console.log($('table.infobox', this.responseBody));
    const $ = cheerio.load(this.responseBody);
    const info = $('table.infobox');

    if (info.length) {
      const tbody = info[0].children[0];
      // console.log(info[0].children);
      tbody.children.forEach((row) => {
        // console.log(index);
        const th = row.children[0] || null;
        const td = row.children[1] || null;

        if (th && td) {
          const rowTitle = this.getRowText(th) || null;
          const rowData = this.getRowText(td) || null;

          // console.group('Parse ');
          // console.info('rowTitle', rowTitle);
          // console.info('rowData', rowData);

          if (rowTitle.length > 0 && rowData.length > 0) {
            switch (rowTitle[0]) {
              case 'Height':
                this.setHeight(rowData[0]);
                break;
              case 'Weight':
                this.setWeight(rowData[0]);
                break;
              case 'Position':
                this.setPosition(rowData[0]);
                break;
              case 'Shot':
                this.setShot(rowData[0]);
                break;
              case 'Born':
                this.setBorn(this.detectDate(rowData));
                break;
            }
          }
          // console.groupEnd('Parse ');
        }
      });
    } else {
      this.error = 'No table';
    }
  }

  init() {
    if (this.responseBody) {
      this.parse();
      // this.playerInfo.show();
    } else {
      console.log('empty body');
    }
  }

  detectDate(list) {
    let res = null;
    // console.dir(list, {depth: 2});

    for (const item of list) {
      let dt = DateTime.fromFormat(item, 'MMMM dd, yyyy');
      if (dt.isValid) {
        res = item;
        break;
      }
      dt = DateTime.fromFormat(item, 'dd MMMM, yyyy');
      if (dt.isValid) {
        res = item;
        break;
      }
      dt = DateTime.fromFormat(item, 'dd LLLL yyyy', {locale: 'en-US'});
      if (dt.isValid) {
        res = item;
        break;
      }
      dt = DateTime.fromFormat(item, 'd LLLL yyyy', {locale: 'en-US'});
      if (dt.isValid) {
        res = item;
        break;
      }
      dt = DateTime.fromFormat(item, 'dd MMMM yyyy');
      if (dt.isValid) {
        res = item;
        break;
      }
      dt = DateTime.fromFormat(item, 'MMMM dd yyyy');
      if (dt.isValid) {
        res = item;
        break;
      }
    }

    return res;
  }
}

module.exports = HandlerWiki;
