'use strict';

const Player = require('./player');

class Handler {
  constructor(names) {
    this.responseBody = null;
    this.names = null;
    this.playerInfo = [];
    if (this.schema(names)) {
      this.names = names;
      this.playerInfo.push(new Player(names));
    } else {
      this.playerInfo.push(null);
    }
    this.error = null;
    this.links = [];
    this.playersCount = 0;
  }

  normalzeHeight(str) {
    let result = 0;

    if (str && str.indexOf('cm') > -1) {
      const value = str.split('cm')[0].trim();
      result = (!isNaN(Number(value))) ? Number(value) : 0;
    }

    return result;
  }

  normalzeWidth(str) {
    let result = 0;

    if (str && str.indexOf('kg') > -1) {
      const value = str.split('kg')[0].trim();
      result = (!isNaN(Number(value))) ? Number(value) : 0;
    }

    return result;
  }

  getParentheses(str) {
    const result = [];
    const myRe = new RegExp('[^`()]+', 'g');
    let myArray = myRe.exec(str);

    while (myArray && myArray.length > 0) {
      let msg = 'Found ' + myArray[0] + '. ';
      result.push(myArray[0]);
      msg += 'Next match starts at ' + myRe.lastIndex;
      // console.log(msg);
      myArray = myRe.exec(str);
    }

    return result;
  }

  setResponseBody(responseBody) {
    this.responseBody = responseBody;
  }

  setHeight(stringHeight) {
    const height = (stringHeight.indexOf('(') > -1 && stringHeight.indexOf(')') > -1) ?
      this.getParentheses(stringHeight) : ['', stringHeight];
    this.playerInfo[this.playersCount].setHeight(this.normalzeHeight(height[1]));
  }

  setWeight(stringWeight) {
    const weight = (stringWeight.indexOf('(') > -1 && stringWeight.indexOf(')') > -1) ?
      this.getParentheses(stringWeight) : ['', stringWeight];
    this.playerInfo[this.playersCount].setWeight(this.normalzeWidth(weight[1]));
  }

  setPosition(stringPosition) {
    this.playerInfo[this.playersCount].setPosition(stringPosition || '');
  }

  setShot(stringShot) {
    this.playerInfo[this.playersCount].setShot(stringShot || '');
  }

  setBorn(stringBorn) {
    this.playerInfo[this.playersCount].setBorn(stringBorn || null);
  }

  setUrl(url) {
    this.playerInfo[this.playersCount].setSource(url || null);
  }

  schema(names) {
    let res = false;

    if (names && names.firstName && names.lastName) {
      res = true;
    }

    return res;
  }
}

module.exports = Handler;
