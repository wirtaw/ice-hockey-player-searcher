'use strict';

const cheerio = require('cheerio');

const Handler = require('./handler');
const Player = require('./player');

class HandlerHockeyDB extends Handler {
  parseUrls(req, baseUrl) {
    // console.log($('div.tablebgl', this.responseBody).length);
    // console.dir($('div.tablebgl', this.responseBody), {depth: 1});
    // console.log($('#sortableTable', this.responseBody).length);
    // console.log($('#myform', this.responseBody).length);
    const $ = cheerio.load(this.responseBody);
    const form = $('#myform');

    if (form && form.length > 0) {
      let table = null;

      form[0].children.forEach(item => {
        if (item.name === 'table') {
          table = item;
        }
      });

      if (table) {
        let tbody = null;

        table.children.forEach(item => {
          if (item.name === 'tbody') {
            tbody = item;
          }
        });

        // console.log(tbody, {depth: 1});

        if (tbody) {
          const rows = [];

          tbody.children.forEach(item => {
            if (item.name === 'tr') {
              rows.push(item);
            }
          });

          if (rows.length > 0) {
            rows.forEach(row => {
              const link = $('a', row);
              if (link && link[0] && link[0].attribs) {
                this.links.push(link[0].attribs.href);
                setTimeout(() => {
                  req.request(`${baseUrl}${link[0].attribs.href}`, this, 2);
                }, 1500);
              }
            });
          } else {
            this.error = 'No table body rows';
          }
        } else {
          this.error = 'No table body';
        }
      } else {
        this.error = 'No table';
      }
    } else {
      this.error = 'No form';
    }
  }

  parse() {
    // console.log($('div.vw .v1-1', this.responseBody).length);
    const $ = cheerio.load(this.responseBody, {
      withDomLvl1: true,
      normalizeWhitespace: true,
      xmlMode: false,
      decodeEntities: true,
    });
    const card = $('div.v1-1');
    if (card && card[0]) {
      // console.group('parseHockey DB');
      const lines = this.getLinesInBlock(card[0]);
      this.getDataInLines(lines);
      // console.dir(lines, {depth: 1});
      // console.groupEnd('parseHockey DB');
    }
  }

  getLinesInBlock(block) {
    const res = [];

    if (block && block.children) {
      for (const item of block.children) {
        if (item.name !== 'br') {
          res.push(item);
        }
      }
    }

    return res;
  }

  getDataInLines(lines) {
    if (lines && lines.length > 0) {
      for (const row of lines) {
        if (row.type === 'text') {
          const data = row.data.trim();

          if (data) {
            const position = this.getPosition(data);
            const shots = this.getShots(data);

            if (shots) {
              this.setShot(shots);
            }

            if (position) {
              this.setPosition(position);
            }
          }
        } else if (row.type === 'tag') {
          if (row.name === 'span' && row.attribs.class === 'bdate') {
            this.setBorn(this.getBirthDate(row.children));
          } else if (row.name === 'span' && row.attribs.class === 'vitals_metric') {
            const metrics = this.getMetrics(row.children);
            this.setHeight(metrics.height);
            this.setWeight(metrics.width);
          }
        }
      }
    }
  }

  getPosition(str) {
    let res = null;

    if (str.indexOf('--') > -1 && (str.indexOf('Left') > -1 ||
      str.indexOf('Right') > -1 ||
      str.indexOf('Center') > -1 ||
      str.indexOf('Defence') > -1 ||
      str.indexOf('Goaltender') > -1)) {
      res = str.split('--')[0].trim();
    }

    return res;
  }

  getShots(str) {
    let res = null;

    if (str.indexOf('--') > -1 && str.indexOf('shoots') > -1) {
      res = str.split('--')[1].trim() === 'shoots L' ? 'Left' : 'Right';
    }

    return res;
  }

  getMetrics(elem) {
    const res = {width: 0, height: 0};

    if (elem[0] && elem[0].data.indexOf('/') > -1 &&
      elem[0].data.indexOf('[') > -1 &&
      elem[0].data.indexOf(']') > -1) {
      res.height = elem[0].data.split('/')[0]
        .replace('[', '')
        .trim();
      res.width = elem[0].data.split('/')[1]
        .replace(']', '')
        .trim();
    }

    return res;
  }

  getBirthDate(elem) {
    let res = null;

    if (elem[0] && elem[0].data.indexOf('--') > -1 && elem[0].data.indexOf('Born') > -1) {
      res = elem[0].data.split('--')[0].replace('Born', '').trim();
    }

    return res;
  }

  init(req, step = 1, baseUrl) {
    if (this.responseBody) {
      if (step === 1) {
        this.parseUrls(req, baseUrl);
      } else if (step === 2) {
        this.parse();
        this.playersCount = this.playersCount + 1;
        this.playerInfo.push(new Player(this.names));
      }
    } else {
      console.log('empty body');
    }
  }
}

module.exports = HandlerHockeyDB;
