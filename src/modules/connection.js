'use strict';

const https = require('https');
const http = require('http');
const mUrl = require('url');
const async = require('async');
// const fs = require('fs');
// const HttpProxyAgent = require('http-proxy-agent');
const HandlerWiki = require('./handlerWiki');
const HandlerHockeyDB = require('./handlerHockeyDB');
const HandlerHockeyReference = require('./handlerHockeyReference');
const Player = require('./player');

class Connection {
  constructor(name = '') {
    this.names = this.isValid(name) ? this.setName(name) : null;

    // wiki
    this.baseUrlWiki = 'https://en.wikipedia.org/wiki/';
    this.urlWiki = null;
    this.handlerWiki = new HandlerWiki(this.names);

    // hockeyDB
    this.baseUrlHockeyDB = 'https://www.hockeydb.com/';
    this.urlHockeyDB = null;
    this.handlerHockeyDB = new HandlerHockeyDB(this.names);

    // hockey-reference
    this.baseUrlHockeyReference = 'https://www.hockey-reference.com/';
    this.urlHockeyReference = null;
    this.handlerHockeyReference = new HandlerHockeyReference(this.names);
  }

  async request(url) {
    const result = {statusCode: null, body: ''};
    const options = mUrl.parse(url);

    options.headers = {
      'User-Agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:71.0) Gecko/20100101 Firefox/71.0',
      Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
      'Accept-Language': 'pl,ru-RU;q=0.8,lt;q=0.7,ru;q=0.5,en-US;q=0.3,en;q=0.2',
      Referer: 'http://www.google.com',
      Connection: 'close',
      DNT: 1,
      'Upgrade-Insecure-Requests': 1,
    };

    const lib = (options.protocol && options.protocol === 'https:') ? https : http;

    return new Promise((resolve, reject) => {
      const req = lib.request(options, (res) => {
        let data = '';
        result.statusCode = res.statusCode;

        res.setEncoding('utf8');

        res.on('data', (chunk) => {
          data += chunk;
        });

        res.on('end', () => {
          // console.info('data:');
          result.body = data;
          resolve(result);
        });
      });

      req.on('error', (e) => {
        console.error(e);
        reject(result);
      });

      req.end();
    });
  }

  setName(fullName) {
    const names = {firstName: '', lastName: ''};
    if (fullName.indexOf(' ') > -1) {
      const [firstName, lastName] = fullName.split(' ');
      names.firstName = firstName;
      names.lastName = lastName;
    } else {
      names.lastName = fullName;
    }
    return names;
  }

  setUrl(url, type = 'wiki') {
    if (url) {
      switch (type) {
        case 'wiki':
          this.urlWiki = url;
          break;
        case 'hockeydb':
          this.urlHockeyDB = url;
          break;
        case 'hockeyreference':
          this.urlHockeyReference = url;
          break;
      }
    } else {
      this.urlWiki = null;
      this.urlHockeyDB = null;
      this.urlHockeyReference = null;
    }
  }

  prepareWikiUrl(iceHockey) {
    const iceHockeyStr = (iceHockey) ? '_(ice_hockey)' : '';
    // eslint-disable-next-line max-len
    return this.names ? encodeURI(`${this.baseUrlWiki}${this.names.firstName}_${this.names.lastName}${iceHockeyStr}`) : null;
  }

  async getWiki(iceHockey) {
    this.setUrl(this.prepareWikiUrl(iceHockey), 'wiki');

    const {statusCode, body} = await this.request(this.urlWiki);

    return new Promise((resolve, reject) => {
      if (statusCode === 200) {
        this.handlerWiki.setResponseBody(body);
        this.handlerWiki.init(this, 1, null);

        resolve();
      } else if (statusCode === 404) {
        // console.dir(err, {depth: 0});
        // console.warn(`Error 404  page not found ${this.urlWiki}`);
        this.error = new Error('page not found');
        reject();
      } else {
        // console.warn(`Status ${statusCode} ${this.urlWiki}`);
        reject();
      }
    });
  }

  prepareHockeyDBUrl() {
    // eslint-disable-next-line max-len
    return `${this.baseUrlHockeyDB}/ihdb/stats/find_player.php?full_name=${this.names.firstName}+${this.names.lastName}`;
  }

  prepareHockeyReferenceUrl() {
    // eslint-disable-next-line max-len
    return this.names && this.names.lastName ? encodeURI(`${this.baseUrlHockeyReference}players/${this.names.lastName[0].toLowerCase()}`) : null;
  }

  async getHockeyDB(callback) {
    this.setUrl(this.prepareHockeyDBUrl(), 'hockeydb');
    await this.request(this.urlHockeyDB, this.handlerHockeyDB, 1, this.baseUrlHockeyDB);

    callback();
  }

  async getPlayerInfo(outerCallback) {
    // console.warn(`getPlayerInfo`);
    const that = this;
    const res = [];
    that.handlerWiki.playersCount = 0;
    that.handlerHockeyReference.playersCount = 0;

    const processData = (statusCode, body, counter) => {
      const data = {statusCode: statusCode, info: {}, error: null};

      console.info(`playersCount ${that.handlerWiki.playersCount} counter ${counter} statusCode ${statusCode}`);

      if (statusCode === 200) {
        if (counter < 3) {
          that.handlerWiki.playerInfo.push(new Player(that.names));

          that.handlerWiki.setResponseBody(body);
          that.handlerWiki.init(that, 1, null);
          data.info = JSON.parse(JSON.stringify(that.handlerWiki.playerInfo));
          that.handlerWiki.playersCount++;
        } else if (counter === 3) {
          that.handlerHockeyReference.playerInfo.push(new Player(that.names));
          that.handlerHockeyReference.playersCount++;
        }
      } else if (statusCode === 404) {
        // console.dir(err, {depth: 0});
        // console.warn(`Error 404  page not found ${that.urlWiki}`);
        that.error = new Error('page not found');
        data.error = `Error 404  page not found ${that.urlWiki}`;
      } else {
        // console.warn(`Status ${statusCode} ${that.urlWiki}`);
        data.error = `Status ${statusCode} ${that.urlWiki}`;
      }

      return data;
    };

    async.waterfall([
      async function() {
        that.setUrl(that.prepareWikiUrl(true), 'wiki');
        console.info(`this.urlWiki ${that.urlWiki} `);
        if (that.urlWiki) {
          const {statusCode, body} = await that.request(that.urlWiki);
          res.push(processData(statusCode, body, 1));
        }

        await Promise.resolve();
      },
      async function() {
        that.setUrl(that.prepareWikiUrl(false), 'wiki');
        console.info(`this.urlWiki ${that.urlWiki} `);

        if (that.urlWiki) {
          const {statusCode, body} = await that.request(that.urlWiki);
          res.push(processData(statusCode, body, 2));
        }

        Promise.resolve();
      },
      async function() {
        that.setUrl(that.prepareHockeyReferenceUrl(), 'hockeyreference');
        console.info(`this.urlHockeyReference ${that.urlHockeyReference} `);

        if (that.urlHockeyReference) {
          const {statusCode, body} = await that.request(that.urlHockeyReference);
          console.info(`this.urlHockeyReference ${that.urlHockeyReference} statusCode ${statusCode}`);
        }
        // console.info(body);
        // res.push(processData(statusCode, body, 2));

        await Promise.resolve();
      },
    ], function(err) {
      if (err) {
        // console.warn(`Error parallel request ${err}`);
        res.push({statusCode: 500, info: null, error: `Error parallel request ${err}`});
      }
      // console.info(`results ${JSON.stringify(results)} res ${JSON.stringify(res)}`);
      outerCallback(res);
    });
  }

  isValid(name) {
    let res = false;

    if (name && name.length > 5 && name.indexOf(' ') > -1) {
      res = true;
    }

    return res;
  }
}

module.exports = Connection;
