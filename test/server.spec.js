'use strict';

const chai = require('chai');
const chaiHttp = require('chai-http');
const cheerio = require('cheerio');
const app = require('./../src/server');

chai.use(chaiHttp);
chai.should();

describe('Server app', () => {
  describe('GET /', () => {
    it(' start app and get index page', done => {
      chai.request(app)
        .get('/')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.type.should.be.eq('text/html');
          done();
        });
    });
  });

  describe('GET /x', () => {
    it(' start app and get not exists page', done => {
      chai.request(app)
        .get('/x')
        .end((err, res) => {
          res.should.have.status(404);
          res.body.should.be.a('object');
          res.type.should.be.eq('text/html');
          done();
        });
    });
  });

  describe('GET /search empty', () => {
    it(' start app and get search empty', done => {
      chai.request(app)
        .get('/search')
        .end((err, res) => {
          const $ = cheerio.load(res.text);
          const errorMessage = $('div.alert-error');

          res.should.have.status(200);
          res.body.should.be.a('object');
          res.type.should.be.eq('text/html');
          errorMessage[0].children[0].data.should.be.eq('Form request is empty');

          done();
        });
    });
  });

  describe('GET /search', () => {
    const playerName = 'Alexander Ovechkin';
    it(' start app and get search success', done => {
      chai.request(app)
        .get('/search')
        .query({playerName})
        .end((err, res) => {
          const $ = cheerio.load(res.text);
          const playerInfo = $('h5.card-title');

          res.should.have.status(200);
          res.body.should.be.a('object');
          res.type.should.be.eq('text/html');
          playerInfo[0].children[0].data.should.be.eq(playerName);
          done();
        });
    });
  });

  describe('POST /search', () => {
    it(' start app and post empty object', done => {
      chai.request(app)
        .post('/search')
        .send({})
        .end((err, res) => {
          const $ = cheerio.load(res.text);
          const errorMessage = $('div.alert-error');

          res.should.have.status(200);
          res.body.should.be.a('object');
          res.type.should.be.eq('text/html');
          errorMessage[0].children[0].data.should.be.eq('Form request is empty');

          done();
        });
    });

    it(' start app and post cyrillic data object', done => {
      const playerName = 'Александр Овечкин';
      chai.request(app)
        .post('/search')
        .send({playerName})
        .end((err, res) => {
          const $ = cheerio.load(res.text);
          const playerInfo = $('h5.card-title');

          res.should.have.status(200);
          res.body.should.be.a('object');
          res.type.should.be.eq('text/html');
          playerInfo.length.should.be.eq(0);

          done();
        });
    });

    it(' start app and post success data object', done => {
      const playerName = 'Alexander Ovechkin';
      chai.request(app)
        .post('/search')
        .send({playerName})
        .end((err, res) => {
          const $ = cheerio.load(res.text);
          const playerInfo = $('h5.card-title');

          res.should.have.status(200);
          res.body.should.be.a('object');
          res.type.should.be.eq('text/html');
          playerInfo[0].children[0].data.should.be.eq(playerName);

          done();
        });
    });
  });

  describe('PUT /', () => {
    it(' try PUT ', done => {
      chai.request(app)
        .put('/')
        .send({})
        .end((err, res) => {
          res.should.have.status(404);
          res.body.should.be.a('object');
          res.type.should.be.eq('text/html');
          done();
        });
    });
  });
});
