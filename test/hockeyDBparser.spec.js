'use strict';

const fs = require('fs');
const {assert} = require('chai');
const HandlerHockeyDB = require('./../src/modules/handlerHockeyDB');

describe('handlerHockeyDB module', () => {
  it(' new HandlerHockeyDB()', () => {
    const handlerHockeyDB = new HandlerHockeyDB();

    assert.equal(handlerHockeyDB.responseBody, null);
    assert.equal(handlerHockeyDB.error, null);
    assert.equal(handlerHockeyDB.playerInfo[handlerHockeyDB.playersCount], null);
  });

  it(' new handlerHockeyDB(\'John Doe\')', () => {
    const handlerHockeyDB = new HandlerHockeyDB('John Doe');

    assert.equal(handlerHockeyDB.responseBody, null);
    assert.equal(handlerHockeyDB.error, null);
    assert.equal(handlerHockeyDB.playerInfo[handlerHockeyDB.playersCount], null);
  });

  it(' new handlerHockeyDB({ firstName:\'John\', lastName: \'Doe\')', () => {
    const handlerHockeyDB = new HandlerHockeyDB({firstName: 'John', lastName: 'Doe'});

    assert.equal(handlerHockeyDB.responseBody, null);
    assert.equal(handlerHockeyDB.error, null);
    assert.equal(typeof handlerHockeyDB.playerInfo[handlerHockeyDB.playersCount], 'object');
  });

  describe('handlerHockeyDB parseUrls methods', () => {
    it(' read player data', done => {
      const handlerHockeyDB = new HandlerHockeyDB({firstName: 'John', lastName: 'Doe'});
      fs.readFile('./test/data/hockeyDBPlayer.html', {encoding: 'utf8'}, (err, data) => {
        handlerHockeyDB.setResponseBody(data);

        assert.equal(handlerHockeyDB.responseBody, data);
        assert.equal(handlerHockeyDB.error, null);
        assert.equal(typeof handlerHockeyDB.playerInfo[handlerHockeyDB.playersCount], 'object');
        done();
      });
    });

    it(' read player data and parse ', done => {
      const name = 'Dickie';
      const lastName = 'Moore';
      const handlerHockeyDB = new HandlerHockeyDB({firstName: name, lastName: lastName});
      fs.readFile('./test/data/hockeyDBPlayer.html', {encoding: 'utf8'}, (err, data) => {
        handlerHockeyDB.setResponseBody(data);
        handlerHockeyDB.parse();

        assert.equal(handlerHockeyDB.responseBody, data);
        assert.equal(handlerHockeyDB.error, null);
        assert.equal(typeof handlerHockeyDB.playerInfo, 'object');
        assert.equal(handlerHockeyDB.playerInfo[handlerHockeyDB.playersCount].name, name);
        assert.equal(handlerHockeyDB.playerInfo[handlerHockeyDB.playersCount].surname, lastName);
        assert.equal(handlerHockeyDB.playerInfo[handlerHockeyDB.playersCount].born, 'Jan 6 1931');
        assert.equal(handlerHockeyDB.playerInfo[handlerHockeyDB.playersCount].height, 178);
        assert.equal(handlerHockeyDB.playerInfo[handlerHockeyDB.playersCount].weight, 84);
        assert.equal(handlerHockeyDB.playerInfo[handlerHockeyDB.playersCount].shot, 'Left');
        assert.equal(handlerHockeyDB.playerInfo[handlerHockeyDB.playersCount].position, 'Left Wing');
        done();
      });
    });
  });
});
