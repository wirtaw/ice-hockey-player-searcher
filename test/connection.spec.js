'use strict';

const {assert} = require('chai');
const Connection = require('./../src/modules/connection');

const BASE_URL_WIKI = 'https://en.wikipedia.org/wiki/';
const BASE_URL_HOCKEY_DB = 'https://www.hockeydb.com/';

describe('Connection module', () => {
  it(' create instance of connection', () => {
    const name = 'Kevin';
    const lastName = 'Stevens';
    const con = new Connection(`${name} ${lastName}`);

    assert.typeOf(con, 'object');
    assert.typeOf(con.names, 'object');
    assert.typeOf(con.names.firstName, 'string');
    assert.typeOf(con.names.lastName, 'string');
    assert.equal(con.names.firstName, name);
    assert.equal(con.names.lastName, lastName);
    assert.equal(con.baseUrlWiki, BASE_URL_WIKI);
    assert.equal(con.baseUrlHockeyDB, BASE_URL_HOCKEY_DB);
    assert.equal(con.urlWiki, null);
    assert.equal(con.urlHockeyDB, null);
  });

  it(' create instance of connection with empty name', () => {
    const con = new Connection('');

    assert.equal(con.names, null);
  });

  it(' create instance of connection without name ', () => {
    const con = new Connection();

    assert.equal(con.names, null);
  });

  it(' create instance of connection without one word set last name ', () => {
    const lastName = 'Stevens';
    const con = new Connection(`${lastName}`);

    assert.equal(con.names, null);
  });

  it(' setUrl method setup wiki url with ice hockey', () => {
    const name = 'Kevin';
    const lastName = 'Stevens';
    const con = new Connection(`${name} ${lastName}`);

    con.setUrl(con.prepareWikiUrl(true));

    assert.equal(con.urlWiki, `${BASE_URL_WIKI}${name}_${lastName}_(ice_hockey)`);
    assert.equal(con.urlHockeyDB, null);
  });

  it(' setUrl method setup wiki url without ice hockey', () => {
    const name = 'Kevin';
    const lastName = 'Stevens';
    const con = new Connection(`${name} ${lastName}`);

    con.setUrl(con.prepareWikiUrl(false));

    assert.equal(con.urlWiki, `${BASE_URL_WIKI}${name}_${lastName}`);
    assert.equal(con.urlHockeyDB, null);
  });

  it(' setUrl method setup wiki url with ice hockey and type', () => {
    const name = 'Kevin';
    const lastName = 'Stevens';
    const con = new Connection(`${name} ${lastName}`);

    con.setUrl(con.prepareWikiUrl(true), 'wiki');

    assert.equal(con.urlWiki, `${BASE_URL_WIKI}${name}_${lastName}_(ice_hockey)`);
    assert.equal(con.urlHockeyDB, null);
  });

  it(' setUrl method setup hockeyDB url', () => {
    const name = 'Kevin';
    const lastName = 'Stevens';
    const con = new Connection(`${name} ${lastName}`);

    con.setUrl(con.prepareHockeyDBUrl(), 'hockeydb');

    assert.equal(con.urlWiki, null);
    assert.equal(con.urlHockeyDB, `${BASE_URL_HOCKEY_DB}/ihdb/stats/find_player.php?full_name=${name}+${lastName}`);
  });

  it(' setUrl method setup wiki and hockeyDB url to null', () => {
    const name = 'Kevin';
    const lastName = 'Stevens';
    const con = new Connection(`${name} ${lastName}`);

    con.setUrl(con.prepareWikiUrl(true), 'wiki');
    con.setUrl(con.prepareHockeyDBUrl(), 'hockeydb');
    con.setUrl(null);

    assert.equal(con.urlWiki, null);
    assert.equal(con.urlHockeyDB, null);
  });

  it(' get player from wiki info successful', (done) => {
    const name = 'Kevin';
    const lastName = 'Stevens';
    const con = new Connection(`${name} ${lastName}`);

    con.getWiki(false).then(() => {
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].name, name);
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].surname, lastName);
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].born, 'April 15, 1965');
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].height, 191);
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].weight, 100);
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].shot, 'Left');
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].position, 'Left Wing');
      done();
    }).catch(() => {
      done();
    });
  });

  it(' get player from wiki info empty', (done) => {
    const name = 'Kevin';
    const lastName = 'Stevens';
    const con = new Connection(`${name} ${lastName}`);

    con.getWiki(true).then(() => {
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].name, name);
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].surname, lastName);
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].born, null);
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].height, 0);
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].weight, 0);
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].shot, '');
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].position, '');
      done();
    }).catch(() => {
      done();
    });
  });

  it(' get player info from wiki page not found', (done) => {
    const name = 'Kevin';
    const lastName = 'Stevens';
    const con = new Connection(`${name} ${lastName}`);

    con.getWiki(true).then(() => {
      done();
    }).catch(() => {
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].name, name);
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].surname, lastName);
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].born, null);
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].height, 0);
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].weight, 0);
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].shot, '');
      assert.equal(con.handlerWiki.playerInfo[con.handlerWiki.playersCount].position, '');
      done();
    });
  });

  it(' get player from hockey DB info successful', (done) => {
    const name = 'Kevin';
    const lastName = 'Stevens';
    const con = new Connection(`${name} ${lastName}`);

    con.getHockeyDB(() => {
      assert.equal(con.handlerHockeyDB.playerInfo[con.handlerHockeyDB.playersCount].name, name);
      assert.equal(con.handlerHockeyDB.playerInfo[con.handlerHockeyDB.playersCount].surname, lastName);
      assert.equal(con.handlerHockeyDB.playerInfo[con.handlerHockeyDB.playersCount].born, null);
      assert.equal(con.handlerHockeyDB.playerInfo[con.handlerHockeyDB.playersCount].height, 0);
      assert.equal(con.handlerHockeyDB.playerInfo[con.handlerHockeyDB.playersCount].weight, 0);
      assert.equal(con.handlerHockeyDB.playerInfo[con.handlerHockeyDB.playersCount].shot, '');
      assert.equal(con.handlerHockeyDB.playerInfo[con.handlerHockeyDB.playersCount].position, '');

      done();
    });
  }).timeout('15s');
});
