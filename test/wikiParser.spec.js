'use strict';

const {assert} = require('chai');
const HandlerWiki = require('./../src/modules/handlerWiki');

describe('handlerWiki module', () => {
  it(' new handleWiki()', () => {
    const handlerWiki = new HandlerWiki();

    assert.equal(handlerWiki.responseBody, null);
    assert.equal(handlerWiki.error, null);
    assert.equal(handlerWiki.playerInfo[handlerWiki.playersCount], null);
  });

  it(' new handleWiki(\'John Doe\')', () => {
    const handlerWiki = new HandlerWiki('John Doe');

    assert.equal(handlerWiki.responseBody, null);
    assert.equal(handlerWiki.error, null);
    assert.equal(handlerWiki.playerInfo[handlerWiki.playersCount], null);
  });

  it(' new handleWiki({ firstName:\'John\', lastName: \'Doe\')', () => {
    const handlerWiki = new HandlerWiki({firstName: 'John', lastName: 'Doe'});

    assert.equal(handlerWiki.responseBody, null);
    assert.equal(handlerWiki.error, null);
    assert.equal(typeof handlerWiki.playerInfo[handlerWiki.playersCount], 'object');
  });
});
