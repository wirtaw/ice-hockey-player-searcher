'use strict';

const {assert} = require('chai');
const Player = require('./../src/modules/player');

describe('Player module', () => {
  it(' new Player()', () => {
    const player = new Player();

    assert.equal(player.name, '');
    assert.equal(player.surname, '');
  });

  it(' new Player(\'John Doe\')', () => {
    const player = new Player('John Doe');

    assert.equal(player.name, '');
    assert.equal(player.surname, '');
  });
});
